#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  BD_XML.py
#
#  Copyright 2014 wolfang Torres <wolfang.Torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import time, datetime, copy, xml.etree.ElementTree as ET
from re import search


class Tabla():
    """Clase de ayuda para importar y exportar informacion entre python, mysql y xml"""
    nombre = ''

    col = []
    fila = []

    valores = []

    xml_texto = ''
    xml_declaration= '<?xml version="1.0" encoding="UTF-8"? standalone="yes"?>'
    xml_arbol = None

    coneccion= {'user':'root', 'password':'', 'db':'', 'host':'127.0.0.1', 'port':'3306'}

    _tipos_variables = {
        'int':'^(small|medium|big)?int(eger)?(\((\d+)\))?( unsigned)?( zerofill)?$',
        'bool':'^tinyint\(1\)$',
        'double':'^(double|real|float|decimal|numeric)(\((\d+)(,\d+)?\))?( unsigned)?( zerofill)?$',
        'date':'^(date/year)$',
        'time':'^time$',
        'datetime':'^(datetime|timestamp)$',
        'varchar':'^(var)?char(\(\d+\))?$',
        'text':'^(tiny|medium|long)?(text|blob)( binary)?$',
        'set':'^(enum|set)\(.*\)$',
        }

    convercion_variables = {
        'date':time,
        'time':time,
        'datetime':time,
        'timestamp':time,
        'varchar':str,
        'char':str,
        'text':str,
        'blob':str,
        'enum':str,
        'set':str,
        'int':int,
        'double':float,
        'float':float,
        'real':float,
        'bool':bool,
        }

    def __init__(self):
        self.mapeo_bd = {}
        try:
            import mysql.connector as MYDB
            self.mapeo_bd['mysql'] = MYDB
        except Exception:
            self.mapeo_bd['mysql'] = None

    def __next__(self):
        if self.pos == len(self.fila):
            raise StopIteration
        else:
            n = self.pos
            self.pos += 1
            return self.valores[n]

    def __iter__(self):
        self.pos = 0
        return self

    def __getitem__(self, key):
        return self.valores[key]

    def __repr__(self):
        return '\n'.join((repr(self.col),repr(self.fila)))

    def __str__(self):
        columnas = [i['campo'] for i in self.col]
        extra = 4

        n = 0
        t = 0
        for i in columnas:
            if len(i) > n: n = len(i)
            t += 1

        formato = ('{:'+str(n)+'}'+(' '*extra))*len(columnas)
        valores = [col.upper().center(n) for col in columnas]
        cols = formato.format(*valores)

        sep = ((('-' * n)  + '-'*extra) * t)

        formato = ('{:'+str(n)+'}'+(' '*extra))*len(columnas)
        valores = []
        for fila in self.fila:
            v = []
            for valor in fila:
                if isinstance(valor,str): val = valor.ljust(n)
                elif isinstance(valor,bool): val = str(valor).center(n)
                elif isinstance(valor,int): val = str(valor).zfill(n)
                else: val = str(valor).center(n)
                v.append(val)
            valores.append(formato.format(*v))

        filas = '\n'.join(valores)

        return '\n'.join((cols, sep, filas))

    def __len__(self):
        return len(self.fila)

    def index(self, val):
        try : n = self.valores.index(obj)
        except IndexError: raise IndexError
        return n

    def agregar_col(self, **arg):
        '''Agrega una columa a la tabla con la información dada o la de por defecto'''
        campo = arg.get('campo',str())
        tipo = arg.get('tipo','varchar')
        nulo = arg.get('nulo','YES')
        llave = arg.get('llave',str())
        defecto = arg.get('defecto',str())
        extra = arg.get('extra',str())
        self.col.append({'campo':campo,'tipo':tipo,'nulo':nulo,'llave':llave,'defecto':defecto,'extra':extra})

    def append(self, *valores):
        self.fila.append(valores)
        self.importar()

    def insert(self, pos, valores):
        self.fila.insert(pos, arg)
        self.importar()

    def copy(self, fila_inicio=None, fila_final=None, salto=None):
        tabla = copy.deepcopy(self)
        tabla.fila =tabla.fila[fila_inicio:fila_final:salto]
        tabla.importar()
        return tabla

    def importar(self):
        '''Guarda los datos de las col y fil en valores o el defecto si estan vacios'''
        self.valores = ()
        l = []
        n = len(self.fila)

        if n > 0:
            for nfila in range(n):
                a = {}
                p = 0
                for val in self.fila[nfila]:
                    a[self.col[p]['campo']] = val
                    p += 1
                l.append(a)
        else:
            a = {}
            for col in self.col:
                a[col['campo']]=col['defecto']
            l.append(a)

        self.valores = l

    def exportar(self):
        '''Guarda los datos de valores en fil o el defecto'''
        t=[]

        if len(self.valores) > 0:
            for fil in self.valores:
                f=[]
                for col in self.col:
                    f.append(fil[col['campo']])
                t.append(f)

        else:
            l = []
            for col in self.col:
                l.append(col['defecto'])
            t.append(l)

        self.fila = t

    def importar_xml(self, xml, **kw):
        """importar_xml(xml, tipo=('texto'/'archivo')
        Crear los datos de la tabla a base de un archivo xml"""
        tipo = kw.get('tipo', 'texto')

        if tipo == 'texto':
            self.xml_texto = xml
            self.xml_tree = ET.XML(xml)
            arbol = self.xml_tree.getroot()
        elif tipo == 'archivo':
            self.xml_tree = ET.parse(xml)
            arbol = self.xml_tree.getroot()
            self.xml_texto = ET.tostring(arbol, encoding='unicode')

        self.nombre = arbol.get('nombre', arbol.tag)

        #LLena la información de las columnas
        self.col = []
        for col in root.find('columnas'):
            campo = col.tag
            tipo = col.get('tipo')
            nulo = col.get('nulo')
            llave = col.get('llave')
            defecto = col.get('defecto')
            extra = col.get('extra')

            self.col.append({
            'campo':campo,
            'tipo':tipo,
            'nulo':nulo,
            'llave':llave,
            'defecto':defecto,
            'extra':extra,
            })

        #LLena la información de la filas
        self.fila = []
        for fila in root.findall('fila'):

            valores = []
            for n in range(len(self.col)):
                if fila[n].text is None:
                    valores.append(None)
                    continue

                for tipo in self._tipos_variables:
                    if search(self._tipos_variables[tipo], self.col['tipo']):
                        if tipo in ('time','date','datetime'):
                            valor = _cambiar_xml_a_time(fila[n])
                        else:
                            valor = self.convercion_variables[tipo](fila[n].text)
                        valores.append(valor)
                        break
                else:
                    valores.append(fila[n].text)

            self.fila.append(tuple(valores))

            self.importar()
            return 0

    def exportar_xml(self, metadatos=True):
        '''Exporta los datos de la tabla a un archivo xml'''
        self.xml = arbol = ET.Element('tabla', nombre=self.nombre)

        #Agrega la estructura de los Encabesados y la Metadata
        if metadatos:
            col = ET.SubElement(arbol,'columnas')
            for col in self.col:
                tipo = col['tipo']
                nulo = col['nulo']
                llave = col['llave']
                defecto = col['defecto']
                extra = col['extra']

                if tipo is None: tipo = ''
                if nulo  is None: nulo = ''
                if llave is None: llave = ''
                if defecto is None: defecto = ''
                if extra is None: extra = ''

                columna = ET.SubElement(col, col['campo'],
                tipo = tipo,
                nulo = nulo,
                llave = llave,
                defecto = defecto,
                extra = extra,
                )

        #Agrega los Datos
        for fil in self.fila:
            fila = ET.SubElement(arbol, "fila")
            for col in self.col:
                val = fil[self.col.index(col)]
                if isinstance(val, type(time.gmtime())): fila.append(_cambiar_time_a_xml(val, col['campo']))
                else:
                    columna = ET.SubElement(fila, col['campo'])
                    columna.text = str(val)

        #Escribe el xml parceado en texto y en el objeto
        self.xml_arbol = ET.ElementTree(arbol)
        self.xml_texto = ET.tostring(arbol, encoding='unicode')


    def obtener_datos(self, con=None, **args):
        '''Importa los datos para conectarse a la base de datos o un dic bien estructurado,
        http://dev.mysql.com/doc/connector-python/en/connector-python-connectargs.html '''
        if not con is None: self.coneccion = con
        else:
            self.coneccion['user'] = args.get('usuario', self.coneccion['user'])
            self.coneccion['password'] = args.get('contraseña', self.coneccion['password'])
            self.coneccion['db'] = args.get('bd', self.coneccion['db'])
            self.coneccion['host']  = args.get('servidor', self.coneccion['host'])
            self.coneccion['port'] = args.get('puerto', self.coneccion['port'])

    def importar_bd(self, tabla=None, cols=('*',), condicion='', cantidad=100, bd='mysql'):
        '''Importa los datos de la tabla desde la base de datos'''
        if bool(tabla): self.nombre = tabla
        BD = self.mapeo_bd[bd]
        self.cnx = BD.connect(**self.coneccion)
        self.cur = self.cnx.cursor(False, True)
        self._col_bd(cols)
        self._fila_bd(condicion, cantidad)

        numero_filas = self.cur.rowcount
        self.importar()
        self.cnx.close()
        return numero_filas

    def exportar_bd(self, nombre_tabla=None, columnas=None, bd='mysql'):
        '''Exporta los datos de la tabla a la base de datos, regresa getlasrowid() por conveniencia'''
        BD = self.mapeo_bd[bd]

        if nombre_tabla is None:
            nombre_tabla = self.nombre
        if columnas is None:
            columnas = [i['campo'] for i in self.col]
        columnas = str(tuple(columnas)).replace("'","`")


        self.cnx = cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        datos = {'tabla':nombre_tabla, 'columnas':columnas, }
        for fil in self.fila:
            fil = tuple(fil)
            datos['valores']=fil
            cur.execute("INSERT INTO `{tabla}` {columnas} VALUES {valores}".format(**datos))

        id = cur.getlastrowid()
        cnx.commit()
        self.cnx.close()
        return id

    def actualisar_bd(self, tabla=None, columnas=None, columnas_llaves=[], bd='mysql'):
        '''Realisa un UPDATE de todas la columnas usando automaticamente las llaves PRI para la clausula WHERE,
        las llaves no tienen que estar dentro de la columnas a actualisar,
        se pueden usar columnas propias como llaves a responsabilidad del usuario,
        regresa ROW_COUNT() por conveniencia'''

        BD = self.mapeo_bd[bd]

        if tabla is None:
            tabla = self.nombre
        if columnas is None:
            columnas = [i['campo'] for i in self.col]

        datos = {'tabla':tabla, }

        #Busca las llaves PRI
        llaves = [i['campo'] for i in self.col if (i['llave'] == 'PRI')]
        llaves = llaves + list(columnas_llaves)
        assert llaves, "La tabla no tiene ninguna columna PRI o columna que usar como llave"

        self.cnx = cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        for val in self.valores:
            #Prepara str con los datos a actualisar
            valores = {}
            for col in columnas:
                valores['`{}`'.format(col)]=val[col]
            valores = str(valores).replace("'","").replace(":","=")[1:-1]
            datos['valores'] = valores

            #Prepara las clausulas WHERE
            datos['condicion'] = _crear_condicion_de_llaves(llaves, val)
            cur.execute("UPDATE `{tabla}` SET {valores} WHERE {condicion}".format(**datos))

        numero_filas = cur.rowcount
        cnx.commit()
        cnx.close()
        return numero_filas

    def borrar_bd(self, tabla=None, columnas_llaves=[], bd='mysql'):
        '''Realisa un DELETE usando automaticamente las llaves PRI para la clausula WHERE,
        se pueden usar columnas propias como llaves a responsabilidad del usuario,
        regresa ROW_COUNT() por conveniencia'''
        BD = self.mapeo_bd[bd]

        if tabla is None:
            tabla = self.nombre

        datos = {'tabla':tabla, }

        #Busca las llaves PRI
        llaves = [i['campo'] for i in self.col if (i['llave'] == 'PRI')]
        llaves = llaves + list(columnas_llaves)
        assert llaves, "La tabla no tiene ninguna columna PRI o columna que usar como llave"

        self.cnx = cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        for fila in self.fila:
            #Prepara las clausulas WHERE
            datos['condicion'] = _crear_condicion_de_llaves(llaves, val)

            cur.execute("DELETE FROM `{tabla}` WHERE {condicion}".format(**datos))

        numero_filas = cur.rowcount
        cnx.commit()
        cnx.close()
        return numero_filas

    def _col_bd(self, cols=None):
        meta = ('campo','tipo','nulo','llave','defecto','extra')
        self.cur.execute('SHOW COLUMNS FROM {}'.format(self.nombre))
        con = self.cur.fetchall()
        self.col = []
        for i in con:
            if (cols is None) or ('*' in cols and not str(i[0],'utf-8') in cols) or (str(i[0],'utf-8') in cols and not '*' in cols):
                col = {}
                for n in range(6):
                    try: atrib = str(i[n],'utf-8')
                    except TypeError: atrib = None
                    col[meta[n]] = atrib
                self.col.append(col)

    def _fila_bd(self, condicion, cantidad):
        col = [i['campo'] for i in self.col]
        col = str(tuple(col))[1:-1].replace("'","`")
        if col[-1] == ',': col = col[:-1]

        self.cur.execute("SELECT {} FROM {} {}".format(col, self.nombre, condicion))
        con = self.cur.fetchmany(cantidad)
        self.fila = con
        self._convertir_datos()

    def _convertir_datos(self):
        tipo = [i['tipo'] for i in self.col]
        t = []
        for fil in self.fila:
            n = 0
            f = []
            for val in fil:
                if val is None:
                    f.append(None)
                else: f.append(self._cambiar_tipo_dato(val,tipo[n]))
                n += 1
            t.append(tuple(f))
        self.fila = t


    def escribir_xml(self, ubicacion):
        '''Escribe los datos xml a un archivo'''
        archivo = open(ubicacion, mode='w')
        self.xml_arbol.write(archivo, encoding='unicode')

    def obtener_info(self, nombre_columna, *info):
        '''Regresa la información de una columna (tipo, nulo, llave, defecto, extra),
        Si se pide una caracteristica se regresa un valor, si se piden mas regresa un tuple,
        regresa None si no hay un acolumna'''
        for col in self.col:
            if col['campo'] == nombre_columna:
                if len(info) > 1:
                    i = [col[carac] for carac in info]

                else: i = col[info[0]]

                return i
        return None

    def _cambiar_tipo_dato(self, valor, tipo):
        valor = str(valor,'utf-8')
        for p in self._tipos_variables:
            if search(self._tipos_variables[p], tipo):
                t = self.convercion_variables[p]
                if t is str: return valor
                elif t is time:
                    s = search('(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)', valor)
                    return time.struct_time([int(s.group(n)) for n in range(1,7)] + [0]*3)
                elif t is int: return int(valor,10)
                elif t is float: return float(valor)
                elif t is bool: return bool(int(valor))
                #Mas converciones a otros formatos
                else:
                    try: return t(valor)
                    except ValueError: raise ValueError('La funcion usada no es capas de convertir los datos')
        return None

estructura_time = {
    0:'año',
    1:'mes',
    2:'dia',
    3:'hora',
    4:'minuto',
    5:'segundo',
    6:'dia_semana',
    7:'dia_año',
    8:'hora_de_verano',
    }

def _cambiar_time_a_xml(objeto_struct_time, tag):
    nodo = ET.Element(tag)
    print('objeto time.struc_time encontrado')

    for n in range(9):
        subnodo = ET.SubElement(nodo, estructura_time[n])
        subnodo.text = str(objeto_struct_time[n])

    return nodo

def _cambiar_xml_a_time(nodo):
    datos= [int(nodo[n].text) for n in range(9)]
    return time.struct_time(datos)

def _crear_condicion_de_llaves(llaves, valores):
    sep = 'AND'
    formato = '{a} = {b}'
    condicion = {}
    for llave in llaves:
        condicion['`{}`'.format(llave)] = valores[llave]
    items = tuple(condicion.items())
    clausulas =[]
    for a, b in items:
        texto = formato.format(**{'a':a, 'b':b})
        clausulas.append(texto)
        if items.index((a, b)) == len(items)-1: break
        clausulas.append(sep)

    return ' '.join(clausulas)


if __name__ == '__main__':
    personas = p = Tabla()
    p.nombre='personas'
    p.agregar_col(campo='id',tipo='int', llave='pri')
    p.agregar_col(campo='nombre',tipo='str')
    p.agregar_col(campo='apellido',tipo='str')
    p.fila.append((1,'wolfang','torres'))
    p.fila.append((2,'wendy','torres'))
    p.fila.append((3,'carlos','molano'))
    p.importar()
    print(p)

    del p.fila[1]
    p.importar()
    print(p)
