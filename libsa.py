#  libsa.py
#
#  Copyright 2014 Wolfang Torres <wolfang.torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import tkinter, sys, os, time, xml.etree.ElementTree as ET
from xml.etree.ElementTree import Element, ElementTree, ProcessingInstruction as PI
import pygubu, BD_XML as bd
from tkinter import ttk, messagebox, Tk, Toplevel

formato_fecha_usuario = "%d/%m/%y %I:%M %p"
formato_fecha_iso = "%Y-%m-%d %H:%M:%S"
formato_fecha = "%d/%m/%Y"
formato_hora = "%H:%M:%S"
formato_fecha_hora = formato_fecha  + ' ' + formato_hora

carpeta_madre = os.path.abspath(os.path.dirname(__file__))
carpeta_recursos = os.path.join(carpeta_madre, 'Recursos')
carpeta_guis = os.path.join(carpeta_madre, 'GUIs')
carpeta_reportes = os.path.join(carpeta_madre, 'Reportes')

class Ventana:
    gui = None
    master = None
    archivo_gui = None
    menu = None

    def __init__(self, master, archivo_gui, menu=None):
        if master is None: master = self.master
        self.archivo_gui = archivo_gui

        if sys.platform.startswith('linux'): self.style = 'clam'
        else: self.style = 'default'

        self._crear_gui(archivo_gui)

        if not menu is None:
            self._crear_menu(menu)
        self.__gui__.connect_callbacks(self)

    def _crear_gui(self, GUI):
        #Contrustor
        self.__gui__ = pygubu.Builder()

        self.__gui__.add_resource_path(carpeta_recursos)

        self.__gui__.add_from_string(GUI)

        #Widgets
        self.vp = self.__gui__.get_object('ventana_principal', self.master)

        self.master.rowconfigure(0, weight=1)
        self.master.columnconfigure(0, weight=1)

    def _crear_menu(self, menu):
        self.menu = self.__gui__.get_object(menu)

        self.master.configure(menu=self.menu)


class Formulario:

    funciones_guardar = tuple()
    funciones_cancelar = tuple()
    funciones_reiniciar = tuple()

    def __init__(self, tabla=tuple(), **funciones):
        self.tablas = tabla
        self.mapeo_text_variable = mapa = {}

        for t in self.tablas:
            m = self.crear_mapa_text(t)
            for i in m:
                mapa[i] = m[i]

        self.funciones_guardar = funciones.pop('guardar',self.funciones_guardar)
        self.funciones_cancelar = funciones.pop('cancelar',self.funciones_cancelar)
        self.funciones_reiniciar = funciones.pop('reiniciar',self.funciones_reiniciar)


    def sincronisar(self, dir='tabla', reg=0, tabla=None, *opciones):
        ''' sincronisar (dir = ( tabla / formulario ), reg=0, tabla=<objeto Tabla>)
        Sincronisa la tabla/formulario con los contenidos del otro.

        Opciones:
            -no_pri: ignora la columna si es primaria
            -none_a_str: comvierte los NoneType a str()
            -fecha: comvierte los time.struc_time a formato'''

        if tabla is None: tabla = self.tabla
        no_pri = 'no_pri' in opciones
        none_a_str = 'none_a_str' in opciones
        fecha = 'fecha' in opciones

        tabla.importar()

        if dir == 'tabla':
            if reg > len(tabla)-1:
                v = (None,)*len(self.tabla.col)
                tabla.append(*v)

            for col in tabla[reg]:
                if no_pri and tabla.obtener_info(col, 'llave') == 'PRI': continue

                tabla[reg][col] = self.__gui__.get_variable(col).get()

                tabla.exportar()

        elif dir == 'formulario':
            for col in tabla[reg]:
                d = tabla[reg][col]

                if no_pri and tabla.obtener_info(col, 'llave') == 'PRI': continue
                if none_a_str and d is None: d = ''
                if isinstance(d, time.struct_time) and fecha: d = time.strftime(formato_fecha_usuario, d)

                self.__gui__.get_variable(col).set(d)

            self.colocar_text_variable()


    def _cambiar_estado(self, widgets=('Entry','Combobox','Radiobutton','Checkbutton', 'Text', 'Spinbox'), estado='disabled', gui=None):
        if gui is None: gui = self.__gui__

        for w in gui.objects:
            for t in widgets:
                if t in str(gui.objects[w]): gui.get_object(w)['state'] = estado

    def _obtener_datos(self, gui=None):
        if gui is None: gui = self.__gui__

        datos = {}

        for var, tkvar in gui.tkvariables.items():
            datos[var] = tkvar.get()

        return datos


    def _cambiar_texto(self, widgets, texto):
        for (w, t) in zip(widgets, texto):
            self.__gui__.get_object(w)['text']=t


    def crear_mapa_text(self, tabla):
        '''Regresa un Dicionario con los tk.Text y sus variables.

        Busca en la Tabla por widgets con nombre de columna,
        verifica si son tk.text, crea una varible con ese nombre,
        y los agrega a un dicionario'''

        mapa = {}
        for col in tabla.col:
            try:
                widget = self.__gui__.get_object(col['campo'])
            except Exception:
                continue

            t = tkinter.Text()
            if type(widget) == type(t):
                variable = self.__gui__.create_variable(col['campo'])
                mapa[widget] = variable

        return mapa


    def colocar_text_variable(self):
        '''Escribe a un widget tk.text el contenido de una StringVar
         segun el mapeo de crear_mapa_text()'''
        m = self.mapeo_text_variable
        for widget in m:
            t = m[widget].get()
            s = False
            if widget['state'] == 'disabled':
                widget['state'] = 'normal'
                s = True
            widget.delete(1.0, 'end')
            widget.insert(1.0, t)
            if s:
                widget['state'] = 'disabled'

    #Bindings
    def obtener_text_variable(self, evento):
        '''Asigna el contenido de un tk.text a una StringVar
        segun el mapeo de crear_mapa_text()'''
        w = evento.widget
        self.mapeo_text_variable[w].set(limpiar_texto(w.get(0.0, 'end')))

    #Callbacks
    def callback(self):
        pass

    def reiniciar(self):
        _ejecutar(self.funciones_reiniciar)

    def cancelar(self):
        _ejecutar(self.funciones_cancelar)
        self.master.destroy()

    def guardar(self):
        _ejecutar(self.funciones_guardar)
        try: self._cambiar_estado()
        except Exception: pass


class Bucle():

    funciones_selecionar = tuple()
    funciones_buscar = tuple()
    funciones_cambio_registro = tuple()

    def __init__(self, gui_bucle, tabla, **funciones):
        self.tabla = tabla
        self.nombre = tabla.nombre
        self.columnas = [col['campo'] for col in tabla.col]
        self.__gui___bucle = gui_bucle
        self.funciones_selecionar = funciones.pop('selecionar', self.funciones_selecionar)
        self.funciones_buscar = funciones.pop('buscar', self.funciones_buscar)
        self.funciones_cambio_registro = funciones.pop('cambio_registro', self.funciones_cambio_registro)

        self.pos = self.__gui___bucle.create_variable('int:_pos_bucle')
        self.tot = self.__gui___bucle.create_variable('int:_tot_bucle')

        try:
            self.boton_sig = self.__gui___bucle.get_object('boton_sig')
            self.boton_ant = self.__gui___bucle.get_object('boton_ant')
            self.boton_buscar = self.__gui___bucle.get_object('boton_buscar')
        except Exception:
            pass

    def _activar(self):
        self.boton_sig['state']='normal'
        self.boton_ant['state']='normal'
        self.boton_buscar['state']='readonly'
        self.pos.set(1)
        self.tot.set(len(self.tabla))
        self.sincronisar('formulario', self.pos.get()-1)
        _ejecutar(self.funciones_cambio_registro)

    def _desactivar(self):
        self.boton_sig['state']='disabled'
        self.boton_ant['state']='disabled'
        self.pos.set(0)
        self.tot.set(0)


#callbacks
    def registro_ant(self):
        p = self.pos.get() -1
        t = self.tot.get() -1
        if p > 0:
            p -= 1
        else:
            p = t
        self.sincronisar('formulario', p, None, 'fecha', 'none_a_str')
        self.pos.set(p+1)
        _ejecutar(self.funciones_cambio_registro)

    def registro_sig(self):
        p = self.pos.get() -1
        t = self.tot.get() -1
        if p < t:
            p += 1
        else:
            p = 0
        self.sincronisar('formulario', p, None, 'fecha', 'none_a_str')
        self.pos.set(p+1)
        _ejecutar(self.funciones_cambio_registro)

    def cambiar_registro(self, numero_registro):
        self.pos.set(numero_registro)
        self.sincronisar('formulario', numero_registro, None, 'fecha', 'none_a_str')
        _ejecutar(self.funciones_cambio_registro)

    def selecionar(self):
        _ejecutar(self.funciones_selecionar)
        self._desactivar()
        self._cambiar_estado()

    def buscar(self):
        _ejecutar(self.funciones_buscar)
        self._cambiar_estado()
        condicion = crear_condicion(self.tabla , self._obtener_datos())

        self.tabla.importar_bd(self.nombre, self.columnas, condicion)

        if bool(self.tabla):    self._activar()

class Reporte:

    clausula_xml = '''version="{version}" encoding="{encoding}"'''
    clausula_estilo = '''type="{type}" href="{href}"'''
    id_estilo = '''stylesheet'''
    tag = '''reporte'''
    encoding = 'UTF-8'
    version = '1.0'

    tablas = []
    hoja_estilo = None
    tipo_hoja_estilo = None

    carpeta_reportes = carpeta_reportes
    carpeta_recursos = carpeta_recursos

    def __init__(self,*tablas, **configuracion):
        self.tablas = tablas

        self.hoja_estilo = configuracion.get('hoja_estilo')

        tipo = 'xsl'
        if '.' in self.hoja_estilo:
            hoja, tipo = self.hoja_estilo.split('.')
            self.tipo_hoja_estilo = tipo

        self.tipo_hoja_estilo = configuracion.get('tipo_hoja_estilo', tipo)


    def reporte_xml_estilo(self):
        nodo_root = datos_xml(*self.tablas)
        nodo_root.tag  = self.tag

        nodo_xml = PI('xml', self.clausula_xml.format(version=self.version, encoding=self.encoding))

        assert  not (self.hoja_estilo is None), 'Falsa hoja de estilo'

        href = os.path.join(self.carpeta_reportes, self.hoja_estilo)
        href = 'FILE://' + href

        tipo = self.tipo_hoja_estilo
        type = '''text/{tipo}'''.format(tipo=tipo)
        nodo_estilo = PI('xml-stylesheet', self.clausula_estilo.format(type=type, href=href))

        xml = (nodo_xml, nodo_estilo, nodo_root)
        return '\n'.join( [ET.tostring(nodo, 'unicode') for nodo in xml] )


def _ejecutar(funciones, list_arg=None):
    if list_arg is None:
        list_arg = ({},)*len(funciones)
    for (fun, arg) in zip(funciones, list_arg):
        fun(**arg)

def crear_condicion(tabla, val):
    """Regresa Una clausula SQL WHERE basado en un objeto Tabla() y dicionario con el nombre de la columna y el valor"""
    ini = 'WHERE'
    temp_str = "`{campo}` LIKE '%{valor}%'"
    temp_int = "`{campo}` = {valor}"
    sep = 'AND'
    esp = ' '
    fin = ''
    defecto = str(True)

    arg = []
    col = tabla.col

    for i in range(len(col)):
        if not bool(val.get(col[i]['campo'],None)):
            continue
        if 'int' in col[i]['tipo']: argumento = temp_int
        else: argumento = temp_str

        arg.append(argumento.format(campo=col[i]['campo'], valor=val[col[i]['campo']]))

    if not bool(arg):
        arg.append(defecto)

    return ini + esp + ('{0}{1}{0}'.format(esp, sep)).join(arg) + fin

def sincronisar_varibles(padre, hijo, variables):
    '''Copia y Pega los valores de las varibles de los objetos'''
    for var in variables:
        hijo.__gui__.get_variable(var).set(padre.__gui__.get_variable(var).get())

def limpiar_texto(texto):
    """_limpiar_texto(texto, banderas) ->text = \n \t abc \n -> \n abc"""
    texto.replace('\t','')
    while texto[-1:] == '\n':
        texto = texto[:-1]
    return texto


def calc_meses(dias):
    meses = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    mes = 0

    while dias >= meses[mes]:
        dias -= meses[mes]
        mes += 1

    return mes, dias

def timedelta_a_iso(timedelta):
    '''convierte un timedate.timedelta a formato iso: YYYY-MM-DD HH:mm:ss, sin extra ceros'''

    formato = "{YYYY}-{MM}-{DD} {HH}:{mm}:{ss}"

    mm, ss = divmod(timedelta.seconds, 60)
    HH, mm = divmod(mm, 60)
    YYYY, DD = divmod(timedelta.days, 365)
    MM, DD = calc_meses(DD)

    return formato.format(**locals())

def capitalisar_palabras(texto):
    l=[]

    for i in texto.split():
        l.append(i.capitalize())

    return ' '.join(l)

def datos_xml(*tablas, con_metadatos=False):
    """Devuelve un objeto Element con los datos de las tablas"""

    xml = ET.Element('xml')
    for n in range(len(tablas)):
        tablas[n].exportar_xml(con_metadatos)
        xml.append(tablas[n].xml_arbol.getroot())

    return xml




