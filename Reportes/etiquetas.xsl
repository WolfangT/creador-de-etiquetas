<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!-- Numero de etiquetas por fila y filas por pagina, deberia ser 4*8 -->
  <xsl:variable name="vNumCols" select="5"/>
  <xsl:variable name="vNumFilas" select="8"/>

  <!-- Diseño deberia ser compatible con US-LETTER: 215.9mm x 279.4mm -->
  <xsl:variable name="ancho_pagina">215.9mm</xsl:variable>
  <xsl:variable name="alto_pagina">279.4mm</xsl:variable>

  <!-- Tamaño se la etiqueta, estandar: 30mm*50mm-->
  <xsl:variable name="alto">30mm</xsl:variable>
  <xsl:variable name="ancho">38mm</xsl:variable>

  <!-- separador entre la etiqueta y el borde de la celda,
  Deberia ser la mitad de lo deseado entre dos etiquetas -->
  <xsl:variable name="separador">2mm</xsl:variable>
  <!-- Teoricamente afecta en tamaño de la etiqueta pero deberia ser negible -->
  <xsl:variable name="borde">2px solid</xsl:variable>

  <xsl:variable name="letra_grande">16px</xsl:variable>
  <xsl:variable name="letra_mediana">14px</xsl:variable>
  <xsl:variable name="letra_normal">10px</xsl:variable>
  <xsl:variable name="letra_pequeña">8px</xsl:variable>


  <xsl:variable name="minusculas" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="mayusculas" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />


<xsl:template match="/">

  <html>

  <head>
    <style>
    *
    {
      font-family: Sans-serif;
      text-align: center;
    }

    .pagina
    {
      width: <xsl:copy-of select="$ancho_pagina" />;
      height: <xsl:copy-of select="$alto_pagina" />;
      border: 1px solid;
    }

    .etiquetario
    {
      width: <xsl:copy-of select="$ancho_pagina" />;
      max-height: <xsl:copy-of select="$alto_pagina" />;
      border-collapse:collapse;
    }

    .linea
    {
      padding: <xsl:copy-of select="$separador" />;
      text-align: center;
    }

    .etiqueta
    {
      width: <xsl:copy-of select="$ancho" />;
      height: <xsl:copy-of select="$alto" />;
      border: <xsl:copy-of select="$borde" />;
    }

    .texto
    {
      font-size: <xsl:copy-of select="$letra_normal" />;
    }

    .detalle
    {
      font-size: <xsl:copy-of select="$letra_pequeña" />;
      font-style:italics;
    }

    .titulo
    {
      font-size :<xsl:copy-of select="$letra_grande" />;
      margin: 5px;
    }

    .valor
    {
      font-size: <xsl:copy-of select="$letra_mediana" />;
      margin: 5px;
    }

    </style>
  </head>

  <body>

    <xsl:for-each select="reporte/tabla[@nombre='Etiquetas']/fila[position() mod ($vNumFilas * $vNumCols) = 1]">

        <xsl:variable name="c_c_nodos" select="/reporte/tabla[@nombre='Etiquetas']/fila[position() mod $vNumCols = 1]"/>
        <xsl:variable name="m_f_nodos" select=". | following-sibling::fila[position() &lt; ($vNumFilas * $vNumCols)]"/>


        <table class="etiquetario" border="1">

            <xsl:for-each select="$c_c_nodos[count(. | $m_f_nodos) = count($m_f_nodos)]">

                <tr class="linea">
                <xsl:apply-templates select=". | following-sibling::fila[position() &lt; $vNumCols]"/>
                </tr>

            </xsl:for-each>

        </table>

  </xsl:for-each>

  </body>

  </html>

</xsl:template>


<xsl:template match="fila">

    <td class="linea">

        <table class="etiqueta" >

            <xsl:if test="not(codigo= '')">
            <tr>
                <th>
                <span class="titulo" ><xsl:value-of select="translate(codigo, $minusculas, $mayusculas)"/></span>
                </th>
            </tr>
            </xsl:if>

            <xsl:if test="not(nombre = '')">
            <tr>
                <td>
                <span class="texto" ><xsl:value-of select="translate(nombre, $minusculas, $mayusculas)"/></span>
                </td>
            </tr>
            </xsl:if>

            <xsl:if test="not(descripcion = '')">
            <tr>
                <td>
                <span class="detalle" ><xsl:value-of select="translate(descripcion, $minusculas, $mayusculas)"/></span>
                </td>
            </tr>
            </xsl:if>

            <xsl:if test="not(costo = '0.00')">
            <tr>
                <th>
                <span class="valor" >
                    <!--<xsl:value-of select="precio/@denominacion" />-->
                    Bs.
                    &#160;
                    <xsl:value-of select="costo"/>
                </span>
                </th>
            </tr>

            <tr>
                <td>
                <span class="detalle" >Precio incluye IVA</span>
                </td>
            </tr>

            <tr>
                <td>
                <span class="detalle" ><xsl:value-of select="fecha"/></span>
                </td>
            </tr>
            </xsl:if>

  </table>

    </td>

</xsl:template>


</xsl:stylesheet>
