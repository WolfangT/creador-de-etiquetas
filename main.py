#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  creador_de_etiqutas
#
#  Copyright 2014 Wolfang Torres <wolfang@canaima-portatil>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import tkinter, time, os, sys, subprocess
from libsa import Ventana, Formulario, Reporte, formato_fecha
from BD_XML import Tabla

archivo_gui = """<?xml version="1.0" ?>
<interface>
  <object class="ttk.Frame" id="ventana_principal">
    <property name="height">200</property>
    <property name="padding">3</property>
    <property name="width">200</property>
    <layout>
      <property name="column">0</property>
      <property name="propagate">True</property>
      <property name="row">0</property>
      <property name="sticky">nesw</property>
      <rows>
        <row id="0">
          <property name="weight">1</property>
        </row>
        <row id="2">
          <property name="weight">0</property>
        </row>
      </rows>
      <columns>
        <column id="0">
          <property name="weight">1</property>
        </column>
      </columns>
    </layout>
    <child>
      <object class="pygubu.builder.widgets.scrollbarhelper" id="scrollbarhelper_1">
        <property name="scrolltype">both</property>
        <layout>
          <property name="column">0</property>
          <property name="propagate">True</property>
          <property name="row">0</property>
          <property name="sticky">nesw</property>
        </layout>
        <child>
          <object class="pygubu.builder.widgets.editabletreeview" id="__tabla__">
            <property name="padding">3</property>
            <property name="selectmode">extended</property>
            <property name="show">headings</property>
            <layout>
              <property name="column">0</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
            </layout>
            <child>
              <object class="ttk.Treeview.Column" id="Column_1">
                <property name="column_anchor">center</property>
                <property name="heading_anchor">center</property>
                <property name="minwidth">20</property>
                <property name="stretch">True</property>
                <property name="text" translatable="yes">Codigo</property>
                <property name="tree_column">False</property>
                <property name="visible">True</property>
                <property name="width">200</property>
              </object>
            </child>
            <child>
              <object class="ttk.Treeview.Column" id="Column_2">
                <property name="column_anchor">center</property>
                <property name="heading_anchor">center</property>
                <property name="minwidth">20</property>
                <property name="stretch">True</property>
                <property name="text" translatable="yes">Nombre</property>
                <property name="tree_column">False</property>
                <property name="visible">True</property>
                <property name="width">200</property>
              </object>
            </child>
            <child>
              <object class="ttk.Treeview.Column" id="Column_3">
                <property name="column_anchor">center</property>
                <property name="heading_anchor">center</property>
                <property name="minwidth">20</property>
                <property name="stretch">True</property>
                <property name="text" translatable="yes">Descripción</property>
                <property name="tree_column">False</property>
                <property name="visible">True</property>
                <property name="width">200</property>
              </object>
            </child>
            <child>
              <object class="ttk.Treeview.Column" id="Column_4">
                <property name="column_anchor">center</property>
                <property name="heading_anchor">center</property>
                <property name="minwidth">20</property>
                <property name="stretch">True</property>
                <property name="text" translatable="yes">Costo</property>
                <property name="tree_column">False</property>
                <property name="visible">True</property>
                <property name="width">200</property>
              </object>
            </child>
            <child>
              <object class="ttk.Treeview.Column" id="Column_5">
                <property name="column_anchor">center</property>
                <property name="heading_anchor">center</property>
                <property name="minwidth">20</property>
                <property name="stretch">True</property>
                <property name="text" translatable="yes">Fecha</property>
                <property name="tree_column">False</property>
                <property name="visible">True</property>
                <property name="width">200</property>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="ttk.Frame" id="Frame_2">
        <property name="height">200</property>
        <property name="padding">3</property>
        <property name="width">200</property>
        <layout>
          <property name="column">0</property>
          <property name="propagate">True</property>
          <property name="row">1</property>
          <property name="sticky">nesw</property>
          <rows>
            <row id="0">
              <property name="weight">1</property>
            </row>
          </rows>
          <columns>
            <column id="0">
              <property name="weight">1</property>
            </column>
            <column id="1">
              <property name="weight">1</property>
            </column>
            <column id="2">
              <property name="weight">0</property>
            </column>
          </columns>
        </layout>
        <child>
          <object class="ttk.Button" id="Button_5">
            <property name="command">eliminar_val</property>
            <property name="text" translatable="yes">-</property>
            <layout>
              <property name="column">0</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">ns</property>
            </layout>
          </object>
        </child>
        <child>
          <object class="ttk.Frame" id="Frame_3">
            <property name="height">200</property>
            <property name="width">200</property>
            <layout>
              <property name="column">1</property>
              <property name="ipadx">3</property>
              <property name="ipady">3</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
                <column id="1">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_5">
                <property name="text" translatable="yes">1</property>
                <property name="textvariable">int:_cantidad</property>
                <property name="validate">none</property>
                <property name="width">3</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">nesw</property>
                </layout>
              </object>
            </child>
            <child>
              <object class="ttk.Button" id="Button_4">
                <property name="command">agregar_val</property>
                <property name="text" translatable="yes">+</property>
                <layout>
                  <property name="column">1</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">nesw</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="ttk.Labelframe" id="Labelframe_1">
        <property name="height">200</property>
        <property name="labelanchor">n</property>
        <property name="padding">3</property>
        <property name="text" translatable="yes">Datos</property>
        <property name="width">200</property>
        <layout>
          <property name="column">0</property>
          <property name="propagate">True</property>
          <property name="row">2</property>
          <property name="sticky">nesw</property>
          <rows>
            <row id="0">
              <property name="weight">0</property>
            </row>
          </rows>
          <columns>
            <column id="0">
              <property name="weight">1</property>
            </column>
            <column id="1">
              <property name="weight">1</property>
            </column>
            <column id="2">
              <property name="weight">1</property>
            </column>
            <column id="3">
              <property name="weight">1</property>
            </column>
          </columns>
        </layout>
        <child>
          <object class="ttk.Labelframe" id="Labelframe_2">
            <property name="height">200</property>
            <property name="labelanchor">n</property>
            <property name="relief">flat</property>
            <property name="text" translatable="yes">Codigo</property>
            <property name="width">200</property>
            <layout>
              <property name="column">0</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_1">
                <property name="textvariable">string:codigo</property>
                <property name="validate">none</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">we</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="ttk.Labelframe" id="Labelframe_3">
            <property name="height">200</property>
            <property name="labelanchor">n</property>
            <property name="relief">flat</property>
            <property name="text" translatable="yes">Nombre</property>
            <property name="width">200</property>
            <layout>
              <property name="column">1</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_2">
                <property name="textvariable">string:nombre</property>
                <property name="validate">none</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">we</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="ttk.Labelframe" id="Labelframe_4">
            <property name="height">200</property>
            <property name="labelanchor">n</property>
            <property name="relief">flat</property>
            <property name="text" translatable="yes">Descripción</property>
            <property name="width">200</property>
            <layout>
              <property name="column">2</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_3">
                <property name="textvariable">string:descripcion</property>
                <property name="validate">none</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">we</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="ttk.Labelframe" id="Labelframe_5">
            <property name="height">200</property>
            <property name="labelanchor">n</property>
            <property name="relief">flat</property>
            <property name="text" translatable="yes">Precio</property>
            <property name="width">200</property>
            <layout>
              <property name="column">3</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_4">
                <property name="textvariable">str:costo</property>
                <property name="validate">none</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">we</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
        <child>
          <object class="ttk.Labelframe" id="Labelframe_7">
            <property name="height">200</property>
            <property name="labelanchor">n</property>
            <property name="padding">3</property>
            <property name="relief">flat</property>
            <property name="text" translatable="yes">Fecha</property>
            <property name="width">200</property>
            <layout>
              <property name="column">4</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
              <rows>
                <row id="0">
                  <property name="weight">1</property>
                </row>
              </rows>
              <columns>
                <column id="0">
                  <property name="weight">1</property>
                </column>
              </columns>
            </layout>
            <child>
              <object class="ttk.Entry" id="Entry_6">
                <property name="textvariable">string:fecha</property>
                <property name="validate">none</property>
                <layout>
                  <property name="column">0</property>
                  <property name="propagate">True</property>
                  <property name="row">0</property>
                  <property name="sticky">we</property>
                </layout>
              </object>
            </child>
          </object>
        </child>
      </object>
    </child>
    <child>
      <object class="ttk.Labelframe" id="Labelframe_6">
        <property name="height">200</property>
        <property name="padding">3</property>
        <property name="text" translatable="yes">Opciones</property>
        <property name="width">200</property>
        <layout>
          <property name="column">0</property>
          <property name="propagate">True</property>
          <property name="row">3</property>
          <property name="sticky">nesw</property>
          <rows>
            <row id="0">
              <property name="weight">1</property>
            </row>
          </rows>
          <columns>
            <column id="0">
              <property name="weight">1</property>
            </column>
            <column id="1">
              <property name="weight">1</property>
            </column>
            <column id="2">
              <property name="weight">1</property>
            </column>
          </columns>
        </layout>
        <child>
          <object class="ttk.Button" id="Button_1">
            <property name="text" translatable="yes">Cerrar</property>
            <layout>
              <property name="column">0</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
            </layout>
          </object>
        </child>
        <child>
          <object class="ttk.Button" id="Button_2">
            <property name="command">borrar_tabla</property>
            <property name="text" translatable="yes">Borrar Tabla</property>
            <layout>
              <property name="column">1</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
            </layout>
          </object>
        </child>
        <child>
          <object class="ttk.Button" id="Button_3">
            <property name="command">imprimir</property>
            <property name="text" translatable="yes">Generar Etiquetas</property>
            <layout>
              <property name="column">2</property>
              <property name="propagate">True</property>
              <property name="row">0</property>
              <property name="sticky">nesw</property>
            </layout>
          </object>
        </child>
      </object>
    </child>
  </object>
</interface>
"""

class Creador_de_Etiquetas(Ventana, Formulario):

    tabla = Tabla()



    lista_filas = []

    pos = 0

    def __init__(self, master):
        self.master = master

        self._tabla()

        Ventana.__init__(self, self.master, archivo_gui)
        self.__tabla__ = self.__gui__.get_object('__tabla__')

        self.fecha = self.__gui__.create_variable('str:fecha')
        self.fecha.set(time.strftime(formato_fecha))
        self.__gui__.get_variable('costo').set('0.00')

        self.cantidad = self.__gui__.get_variable('_cantidad')
        self.cantidad.set(1)

        Formulario.__init__(self, (self.tabla, ) )

        print(self.pos)
        print(self.tabla)

    def _tabla(self):
        self.tabla.nombre = "Etiquetas"
        self.tabla.agregar_col(campo="codigo", tipo="varchar")
        self.tabla.agregar_col(campo="nombre", tipo="varchar")
        self.tabla.agregar_col(campo="descripcion", tipo="varchar")
        self.tabla.agregar_col(campo="costo", tipo="float")
        self.tabla.agregar_col(campo="fecha", tipo="datetime")

    def agregar_val(self):
        cant = self.cantidad.get()
        for i in range(cant):
            self.sincronisar('tabla', self.pos)
            self.pos += 1
            print(self.pos)

        print(self.tabla)
        self.actualisar_tabla()

        for i in self.__gui__.tkvariables:
            self.__gui__.tkvariables[i].set('')
            self.fecha.set(time.strftime(formato_fecha))
            self.cantidad.set(1)
            self.__gui__.get_variable('costo').set('0.00')


    def eliminar_val(self):
        if not bool(self.__tabla__.selection()): return None

        for id in self.__tabla__.selection():
            reg = self.lista_filas.index(id)
            del self.tabla.fila[reg]
            self.pos -= 1

        self.tabla.importar()

        print(self.pos)
        print(self.tabla)

        self.actualisar_tabla()


    def borrar_tabla(self):
        self.tabla.fila = ()
        self.tabla.importar()
        print(self.tabla)
        self.actualisar_tabla()


    def actualisar_tabla(self):
        for i in self.lista_filas:
            self.__tabla__.delete(i)
        self.lista_filas = []

        for reg in self.tabla:
            valores = []
            for col in [col['campo'] for col in self.tabla.col]:
                if isinstance(reg[col], time.struct_time):
                    valores.append(time.strftime(formato_fecha_usuario, reg[col]))
                else:
                    valores.append(reg[col])

            id = self.__tabla__.insert('', 'end', values=valores, tags='fila')
            self.lista_filas.append(id)

    def imprimir(self):
        #crear reporte
        reporte = Reporte(self.tabla, hoja_estilo='etiquetas.xsl')
        xml = reporte.reporte_xml_estilo()
        nombre = '''etiquetas.xml'''
        carpeta = reporte.carpeta_reportes
        dir = os.path.join(carpeta, nombre)
        with open(dir, 'w') as archivo: archivo.write(xml)

        #Abrir el reporte
        if sys.platform.startswith('linux'):
            subprocess.Popen(['firefox', dir])
        if sys.platform.startswith('win'):
            os.startfile(dir)

        #self.__estado__.set('''Reporte "{nombre}" Generado en la carpeta "{carpeta}"'''.format(**locals()))


def main():
    vp = Creador_de_Etiquetas(rais)

    return 0

if __name__ == '__main__':
    rais = tkinter.Tk()
    main()
    rais.mainloop()

